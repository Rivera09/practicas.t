/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica2;

import javax.swing.JFrame;

/**
 *
 * @author mine_
 */
public class Practica2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        LimiteInferiorSuperior panel = new LimiteInferiorSuperior();
        JFrame f = new JFrame("Limite inferior y superior");
        f.setSize(500, 400);
        f.setLocationRelativeTo(f);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(panel);
        f.setVisible(true);
    }
    
}
