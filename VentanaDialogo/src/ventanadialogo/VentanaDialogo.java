package ventanadialogo;

import javax.swing.JFrame;

public class VentanaDialogo {

    public static void main(String[] args) {
        PanelDialogo panel = new PanelDialogo();
        JFrame jFrame = new JFrame();
        jFrame.setSize(400, 200);
        jFrame.setLocationRelativeTo(jFrame);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.add(panel);
        jFrame.setVisible(true);
    }
    
}